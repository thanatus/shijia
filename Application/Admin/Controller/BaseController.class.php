<?php  
namespace Admin\Controller;
use Think\Controller;
class BaseController extends Controller{
	function _initialize(){
		//判断是否登录了管理员账号
		if(!isset($_COOKIE['adminid'])){
			$this->error('请先登录管理员账号',U('Admin/Login/index'));
			exit();
		}
	}
}